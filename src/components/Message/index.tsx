import * as React from 'react';

import './styles.css';

const MONTHS = ['Jan', 'Feb', 'Mar', 'Apr', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

interface MessageProps {
  className?: string;
  message: string;
  timestamp: string;
}

export class Message extends React.Component<MessageProps> {
  constructor(props: MessageProps) {
    super(props);
  }

  render() {
    const { className, message, timestamp } = this.props;
    const date = new Date(timestamp);
    const day = date.getUTCDate();
    const month = MONTHS[date.getUTCMonth()];
    const year = date.getUTCFullYear();

    return (
      <div className={`Message ${className}`}>
        <div className="message-left">
          <div className="user-info" />
        </div>
        <div className="message-body">
          <div className="date">{`${day} ${month}, ${year}`}</div>
          <div className="message">{message}</div>
        </div>
      </div>
    );
  }
}
