import * as React from 'react';

import './styles.css';
import { GhostButton } from '../GhostButton';

interface CardProps {
  className?: string;
  additionalContent?: any;
}

export class Card extends React.Component<CardProps, any> {
  constructor(props: CardProps) {
    super(props);

    this.state = {
      showMore: false,
    };

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState({ showMore: !this.state.showMore });
  }

  renderButton() {
    if (this.state.showMore) {
      return <GhostButton onClick={this.handleClick}>Hide Details</GhostButton>;
    }

    return <GhostButton onClick={this.handleClick}>View Details</GhostButton>;
  }

  render() {
    const { className, additionalContent } = this.props;
    const { showMore } = this.state;

    return (
      <div className={`Card ${className}`}>
        <div className="card-content">
          {this.props.children}
          {showMore && additionalContent ? (
            <div className="additional-content-wrapper">{additionalContent} </div>
          ) : null}
        </div>
        {additionalContent ? <div className="details-action">{this.renderButton()}</div> : null}
      </div>
    );
  }
}
