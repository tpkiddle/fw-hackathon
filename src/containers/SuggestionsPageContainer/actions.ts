import {
  GetRatesStart,
  GetRatesDone,
  GetRatesFail,
  ExecuteStart,
  ExecuteDone,
  ExecuteFail,
  GetTradeStart,
  GetTradeDone,
  GetTradeFail,
  ClearTrade,
} from './actionTypes';
import { RegisteredAction } from 'src/actions';
import { Dispatch } from 'react';

import { getSuggestionRates, executeTrade, getTrade } from 'src/api';
import { ErrorResponse } from 'src/entities';

export function getSuggestedRatesStart(): GetRatesStart {
  return { type: 'GET_RATES_START' };
}

export function getSuggestedRatesDone(rates: any): GetRatesDone {
  return { type: 'GET_RATES_DONE', rates };
}

export function getSuggestedRatesFail(error: ErrorResponse): GetRatesFail {
  return { type: 'GET_RATES_FAIL', error };
}

export function executeStart(): ExecuteStart {
  return { type: 'EXECUTE_START' };
}

export function executeDone(trade: any): ExecuteDone {
  return { type: 'EXECUTE_DONE', trade };
}

export function executeFail(error: ErrorResponse): ExecuteFail {
  return { type: 'EXECUTE_FAIL', error };
}

export function getTradeStart(): GetTradeStart {
  return { type: 'GET_TRADE_START' };
}

export function getTradeDone(tradeCheck: any): GetTradeDone {
  return { type: 'GET_TRADE_DONE', tradeCheck };
}

export function getTradeFail(error: ErrorResponse): GetTradeFail {
  return { type: 'GET_TRADE_FAIL', error };
}

export function clearTrade(): ClearTrade {
  return { type: 'CLEAR_TRADE', data: null };
}

export function performClearTrade() {
  return (dispatch: Dispatch<RegisteredAction>, getState: any, api: any) => {
    dispatch(clearTrade());
  };
}

export function performGetRates() {
  return (dispatch: Dispatch<RegisteredAction>, getState: any, api: any) => {
    dispatch(getSuggestedRatesStart());

    getSuggestionRates()
      .then((response: any) => {
        dispatch(getSuggestedRatesDone(response.data));
      })
      .catch((error: any) => {
        dispatch(getSuggestedRatesFail(error));
      });
  };
}

export function performExecute(amount: string) {
  return (dispatch: Dispatch<RegisteredAction>) => {
    dispatch(executeStart());

    executeTrade(amount)
      .then((response: any) => {
        dispatch(executeDone(response.data));
      })
      .catch((error: any) => {
        dispatch(executeFail(error));
      });
  };
}

export function performGetTrade(id: string) {
  return (dispatch: Dispatch<RegisteredAction>) => {
    dispatch(getTradeStart());

    getTrade(id)
      .then((response: any) => {
        dispatch(getTradeDone(response.data));
      })
      .catch((error: any) => {
        dispatch(getTradeFail(error));
      });
  };
}
