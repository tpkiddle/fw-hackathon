import * as React from 'react';
import { TextField } from '../../components/TextField';
import './styles.css';
import { Redirect } from 'react-router';
import { FetchedData, LedgerAccount } from 'src/entities';

export interface Credentials {
  address: string;
  secret: string;
}

export interface StartPageProps {
  account: FetchedData<LedgerAccount>;
  onSetAccountCredentials: (credentials: { address: string; secret: string }) => void;
}

export interface StartPageState {
  address: string;
  secret: string;
}

export class StartPage extends React.Component<StartPageProps, StartPageState> {
  constructor(props: StartPageProps) {
    super(props);

    this.state = {
      address: 'rsJ6feoUNcgqG8cLCqHqt7Te4id4jKgNX8',
      secret: 'ss6bqAwQTmR6vrCDgdEecXF2wKWu5',
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeAddress = this.handleChangeAddress.bind(this);
    this.handleChangeSecret = this.handleChangeSecret.bind(this);
  }

  handleSubmit(e: React.FormEvent): any {
    const { address, secret } = this.state;
    e.preventDefault();

    if (!address || !secret) {
      return;
    }

    this.props.onSetAccountCredentials({
      address,
      secret,
    });
    return;
  }

  handleChangeAddress(e: React.ChangeEvent<HTMLInputElement>) {
    this.setState({
      address: e.currentTarget.value,
    });
  }

  handleChangeSecret(e: React.ChangeEvent<HTMLInputElement>) {
    this.setState({
      secret: e.currentTarget.value,
    });
  }

  renderBody() {
    return (
      <div className="page_content">
        <h1 className="page_heading">Socially</h1>
        <h1 className="page_sub_heading">
          Get paid for being social on a network that advocates free speech and individuality.
        </h1>
        <p className="help_text">Just enter your private key to get started</p>
        <form onSubmit={this.handleSubmit} className="start-form">
          <TextField
            type="text"
            placeholder="Address"
            className="private-key-field"
            onChange={this.handleChangeAddress}
          />
          <TextField
            type="password"
            placeholder="Secret"
            className="private-key-field"
            onChange={this.handleChangeSecret}
          />
          <button className="btn btn-primary" type="submit">
            Go
          </button>
        </form>
      </div>
    );
  }

  render() {
    const { account } = this.props;

    if (account && account.data) {
      return <Redirect to="/dashboard" />;
    }

    return (
      <div className="StartPage">
        {this.renderBody()}
        <div className="circle-2" />
      </div>
    );
  }
}
