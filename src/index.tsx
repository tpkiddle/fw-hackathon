import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { AnyAction, createStore, compose, applyMiddleware } from 'redux';
import { PersistConfig, persistReducer, persistStore } from 'redux-persist';
import reduxSessionStorage from 'redux-persist/lib/storage/session';
import { rootReducer, RootState } from './rootReducer';
import { AppRootContainer } from './containers/AppRootContainer';
import { persistor as appPersistor } from './containers/AppRootContainer/reducer';
import reduxThunk from 'redux-thunk';
import './index.css';
import { PersistGate } from 'redux-persist/integration/react';
import logger from 'redux-logger';

const persistConfiguration: PersistConfig = {
  key: 'xrpsocial',
  storage: reduxSessionStorage,
  whitelist: ['app'],
  transforms: [appPersistor],
};

const reducer = persistReducer<RootState | void, AnyAction>(persistConfiguration, rootReducer);
const middlewares = [reduxThunk, logger] as any;
const middleware = applyMiddleware(...middlewares);
const enhancers = compose(middleware);
const store = createStore(reducer, enhancers);
const persistor = persistStore(store);

ReactDOM.render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <AppRootContainer />
    </PersistGate>
  </Provider>,
  document.getElementById('root') as HTMLElement
);
