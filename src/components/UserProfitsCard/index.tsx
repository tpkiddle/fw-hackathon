import * as React from 'react';

import './styles.css';

interface UserProfitsCardProps {
  className?: string;
}

export class UserProfitsCard extends React.Component<UserProfitsCardProps> {
  constructor(props: UserProfitsCardProps) {
    super(props);
  }

  render() {
    const { className } = this.props;

    return (
      <div className={`UserProfitsCard ${className}`}>
        <div className="content">
          <div className="content-body">
            <div className="profits-card-title">Total Profit</div>
            <div className="profits-info">
              <div className="amount">13456</div>
              <div className="currency">XRP</div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
