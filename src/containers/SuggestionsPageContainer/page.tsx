import * as React from 'react';
import { FetchedData } from 'src/entities';
import { TopBar } from '../../components/TopBar';
import './styles.css';

import { Heading } from 'src/components/Heading';
import { FauxSuggestionList } from 'src/components/FauxSuggestionList';
import { Suggestion } from 'src/components/Suggestion';
import { TradeForm } from 'src/components/TradeForm';
import { ProgressCard } from 'src/components/ProgressCard';
import { TradeComplete } from 'src/components/TradeComplete';
import { Button } from 'src/components/Button';

export interface SuggestionsPageProps {
  rates: FetchedData<any>;
  executed: FetchedData<any>;
  tradeCheck: FetchedData<any>;
  onGetRates: () => void;
  onExecute: (amount: string) => void;
  onCheck: (id: string) => void;
  onClear: () => void;
}

export interface SuggestionsPageState {
  activeStep: number;
  showTradeForm: boolean;
  tradeInFlight: boolean;
  tradeComplete: boolean;
}

export class SuggestionsPage extends React.Component<SuggestionsPageProps, SuggestionsPageState> {
  constructor(props: SuggestionsPageProps) {
    super(props);

    this.state = {
      activeStep: 0,
      showTradeForm: false,
      tradeInFlight: false,
      tradeComplete: false,
    };

    this.handleClickTrade = this.handleClickTrade.bind(this);
    this.handleClickCancel = this.handleClickCancel.bind(this);
    this.handleClickExecute = this.handleClickExecute.bind(this);
    this.handleClickNewTrade = this.handleClickNewTrade.bind(this);
  }

  componentDidMount() {
    this.props.onGetRates();
  }

  handleClickTrade(e: React.FormEvent) {
    e.preventDefault();

    this.setState({ showTradeForm: true });
  }

  handleClickCancel(e: React.FormEvent) {
    e.preventDefault();
    console.log(this.props.onClear);
    this.props.onClear();
    this.setState({ showTradeForm: false });
  }

  handleClickExecute(value: string) {
    this.props.onExecute(value);
    this.setState({ showTradeForm: false, tradeInFlight: true, tradeComplete: false });
  }

  handleClickNewTrade() {
    this.props.onClear();

    this.setState({ showTradeForm: false, tradeInFlight: false, tradeComplete: false });
  }

  renderSelectedSuggestion() {
    return (
      <React.Fragment>
        <Heading>Suggested Trades</Heading>
        <Suggestion
          selected={true}
          costSavingLabel="XRP is 4% cheaper in USD"
          sourceExchange="Bitstamp"
          sourceCurrency="USD"
          sourceAmount="19.4014"
          destinationExchange="Bitso"
          destinationCurrency="MXN"
          destinationAmount="19.4321"
          onClick={this.handleClickTrade}
        />
        <div className="suggestions-title">All Corridors</div>
        <FauxSuggestionList />;
      </React.Fragment>
    );
  }

  renderFauxSuggestions() {
    return (
      <React.Fragment>
        <div className="suggestions-title">All Corridors</div>
        <FauxSuggestionList />
      </React.Fragment>
    );
  }

  renderTradeForm() {
    return (
      <React.Fragment>
        <Heading>How Much XRP</Heading>
        <TradeForm
          rateDifference="-0.02 XRP"
          onCancel={this.handleClickCancel}
          onSubmit={this.handleClickExecute}
        />
      </React.Fragment>
    );
  }

  timer: any = null;

  componentDidUpdate() {
    const { executed, tradeCheck } = this.props;

    if (executed.data) {
      if (!tradeCheck.data && !this.timer) {
        this.timer = setInterval(() => {
          this.setState({
            activeStep:
              this.state.activeStep <= 2 ? this.state.activeStep + 1 : this.state.activeStep,
          });
          this.props.onCheck(executed.data.id);
        }, 8000);
      }

      if (tradeCheck.data && tradeCheck.data.status === 'COMPLETE' && this.timer) {
        clearInterval(this.timer);
      }
    }
  }

  renderTradeInFlightCard() {
    return (
      <React.Fragment>
        <Heading>Trade in Flight</Heading>
        <ProgressCard activeStep={this.state.activeStep} />
      </React.Fragment>
    );
  }

  renderTradeComplete() {
    return (
      <React.Fragment>
        <TradeComplete />
        <Button onClick={this.handleClickNewTrade}>Make Another Trade</Button>
      </React.Fragment>
    );
  }

  render() {
    const { showTradeForm, tradeInFlight } = this.state;
    const { tradeCheck } = this.props;

    return (
      <div className="SuggestionsPage">
        <TopBar />
        <div className="content">
          <div className="content-body">
            {tradeInFlight && tradeCheck.data && tradeCheck.data.status === 'COMPLETE'
              ? this.renderTradeComplete()
              : null}
            {(tradeInFlight && !tradeCheck.data) ||
            (tradeCheck.data && tradeCheck.data.status !== 'COMPLETE')
              ? this.renderTradeInFlightCard()
              : null}
            {showTradeForm || tradeInFlight ? null : this.renderSelectedSuggestion()}
            {showTradeForm ? this.renderTradeForm() : null}
            {showTradeForm ? null : this.renderFauxSuggestions()}
          </div>
        </div>
      </div>
    );
  }
}
