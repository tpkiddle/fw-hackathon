import { SuggestionsPageContainer } from '../containers/SuggestionsPageContainer';
import { ActivityPageContainer } from '../containers/ActivityPageContainer';
import { BalancesPageContainer } from 'src/containers/BalancesPageContainer';

interface RouteConfig {
  [key: string]: {
    component: any;
  };
}

export const ROUTES_CONFIG: RouteConfig = {
  '/': {
    component: SuggestionsPageContainer,
  },
  '/activity': {
    component: ActivityPageContainer,
  },
  '/accounts': {
    component: BalancesPageContainer,
  },
};
