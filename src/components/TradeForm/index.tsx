import * as React from 'react';

import './styles.css';
import { Button } from '../Button';
import { Card } from '../Card';
import { GhostButton } from '../GhostButton';

interface TradeFormProps {
  className?: string;
  placeholder?: string;
  rateDifference: string;
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  onCancel?: (e: React.MouseEvent<HTMLButtonElement>) => void;
  onSubmit?: (value: string) => void;
}

interface TradeFormState {
  value?: string;
}

export class TradeForm extends React.Component<TradeFormProps, TradeFormState> {
  constructor(props: TradeFormProps) {
    super(props);

    this.state = {
      value: '',
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  static defaultProps = {
    disabled: false,
  };

  handleChange(e: React.ChangeEvent<HTMLInputElement>) {
    const { onChange } = this.props;
    this.setState({
      value: e.currentTarget.value,
    });
    onChange && onChange(e);
  }

  handleCancel(e: React.MouseEvent<HTMLButtonElement>) {
    const { onCancel } = this.props;
    onCancel && onCancel(e);
  }

  handleSubmit(e: React.MouseEvent<HTMLButtonElement>) {
    const { onSubmit } = this.props;
    const { value } = this.state;
    if (!value) {
      return;
    }

    onSubmit && onSubmit(value);
  }

  render() {
    const { className } = this.props;
    const { value } = this.state;

    return (
      <React.Fragment>
        <div className={`TradeForm ${className}`}>
          <Card>
            <div className="top-content">
              <input type="text" value={value} onChange={this.handleChange} />
            </div>
            <div className="middle-content">
              <div className="form-title">Current Rate Difference</div>
              <div className="rate-difference">-0.02 XRP</div>
              <div className="form-title">Expected Profit</div>
              {value ? (
                <div className="expected-profit">PROFIT</div>
              ) : (
                <div className="expected-profit-placeholder">Enter XRP amount</div>
              )}
            </div>
            <div className="bottom-content">
              <Button onClick={this.handleSubmit} disabled={!this.state.value}>
                Execute Trade
              </Button>
              <div>
                <GhostButton onClick={this.handleCancel}>Cancel</GhostButton>
              </div>
            </div>
          </Card>
        </div>
      </React.Fragment>
    );
  }
}
