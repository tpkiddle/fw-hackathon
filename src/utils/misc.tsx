import { FetchedData } from '../entities';

export const createFetchedData = (withProps?: Partial<FetchedData<any>>): FetchedData<any> => {
  return {
    data: null,
    error: null,
    isLoading: false,
    ...withProps,
  };
};
