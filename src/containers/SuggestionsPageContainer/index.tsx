import { connect } from 'react-redux';
import { SuggestionsPage, SuggestionsPageProps } from './page';
import { RootState } from 'src/rootReducer';
import { performGetRates, performExecute, performGetTrade, performClearTrade } from './actions';

const mapStateToProps = (state: RootState): Partial<SuggestionsPageProps> => {
  return {
    rates: state.suggestionsPage.rates,
    executed: state.suggestionsPage.trade,
    tradeCheck: state.suggestionsPage.tradeCheck,
  };
};

const mapDispatchToProps = {
  onGetRates: performGetRates,
  onExecute: performExecute,
  onCheck: performGetTrade,
  onClear: performClearTrade,
};

export const SuggestionsPageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(SuggestionsPage);
