// import { EventEmitter } from 'events';
// import { RippleAPI } from '@aqfr/aqfr-ripple-lib';
// import { AccountInfoResponse } from '@aqfr/aqfr-ripple-lib/dist/npm/common/types/commands';

// const axios = require('axios');
// const WebSocket = require('ws');

// export interface MessagingApiProps extends EventEmitter {
//   server: string;
//   address: string;
//   secret: string;
// }

// class MessagingAPI extends EventEmitter {
//   connection: WebSocket;
//   address: string;
//   secret: string;
//   txId: number;

//   private _api: RippleAPI;

//   constructor(props: MessagingApiProps) {
//     super();

//     this.address = props.address;
//     this.secret = props.secret;

//     this._api = new RippleAPI();

//     this.requestMap = {};
//     this.subscribeCallback = null;
//     this.txId = 0;
//   }

//   requestMap = {};
//   subscribeCallback = null;

//   async subscribe(callback: any) {
//     const request = {
//       id: 'Subscribe to Central',
//       command: 'subscribe',
//       accounts: [this.address],
//     };
//     this.subscribeCallback = callback;
//     this.connection.send(JSON.stringify(request));
//   }

//   async setDomain(sequence, domain, callback) {
//     const txn = {
//       TransactionType: 'AccountSet',
//       Account: this.address,
//       Sequence: sequence,
//       Domain: this._api.convertStringToHex(domain).toUpperCase(),
//       Fee: '20',
//     };
//     const signed = this._api.sign(JSON.stringify(txn), this.secret);

//     const txnObj = {
//       id: this.txId++,
//       command: 'submit',
//       tx_blob: signed.signedTransaction,
//     };

//     this.connection.send(JSON.stringify(txnObj));
//   }

//   async getLedgerAccountInfo(address?: string): Promise<AccountInfoResponse> {
//     const params = {
//       id: this.txId++,
//       account: address || this.address,
//     };

//     return this._api.request('account_info', { ...params });
//   }

//   getLocalRecordsByType(type: string) {
//     return this.getRecordsByType(this.address, type);
//   }

//   async getRecordsByType(address: string, type: string) {
//     const records = [];
//     const accountInfo = await this.getLedgerAccountInfo(address);
//     let prevHash = 'NONE';

//     if (typeof data.result.account_data.Domain !== 'undefined') {
//       const domain = data.result.account_data.Domain;
//       const decodedDomain = this.fromHex(domain);

//       if (decodedDomain.startsWith('xrp://')) {
//         prevHash = decodedDomain.substring(6);
//       }
//     }
//     if (prevHash === 'NONE') {
//       return new Promise(resolve => resolve([]));
//     }

//     return this.nagivateRecordsByType(prevHash, type);
//   }

//   nagivateRecordsByType(prevHash, type, records, callback) {
//     const aqfr = this;
//     this.getTx(prevHash, function(data) {
//       if (typeof data.result !== 'undefined' && typeof data.result.Memos !== 'undefined') {
//         const memo = data.result.Memos[0];
//         const memoType = aqfr.fromHex(memo.Memo.MemoType);
//         const memoData = aqfr.fromHex(memo.Memo.MemoData);
//         const parsedMemoData = JSON.parse(memoData);
//         if (memoType == type) {
//           records.push(parsedMemoData);
//         }
//         if (parsedMemoData.prev_hash != 'xrp://NONE') {
//           var prevHash = parsedMemoData.prev_hash;
//           if (parsedMemoData.prev_hash.startsWith('xrp://')) {
//             prevHash = parsedMemoData.prev_hash.substring(6);
//           }
//           aqfr.nagivateRecordsByType(prevHash, type, records, callback);
//         } else {
//           callback(records);
//         }
//       } else {
//         callback(records);
//       }
//     });
//   }

//   getLocalRecords(callback) {
//     this.getRecords(this.address, callback);
//   }

//   getRecords(address, callback) {
//     const aqfr = this;
//     const records = [];
//     this.getLedgerAccountInfo(address, function(data) {
//       var prevHash = 'NONE';
//       if (typeof data.result.account_data.Domain !== 'undefined') {
//         const domain = data.result.account_data.Domain;
//         const decodedDomain = aqfr.fromHex(domain);

//         if (decodedDomain.startsWith('xrp://')) {
//           prevHash = decodedDomain.substring(6);
//         }
//       }
//       if (prevHash == 'NONE') {
//         console.log('no records found');
//         callback([]);
//       } else {
//         aqfr.nagivateRecords(prevHash, records, callback);
//       }
//     });
//   }

//   nagivateRecords(prevHash, records, callback) {
//     const aqfr = this;
//     this.getTx(prevHash, function(data) {
//       if (typeof data.result !== 'undefined' && typeof data.result.Memos !== 'undefined') {
//         const memo = data.result.Memos[0];
//         const memoType = aqfr.fromHex(memo.Memo.MemoType);
//         const memoData = aqfr.fromHex(memo.Memo.MemoData);
//         const parsedMemoData = JSON.parse(memoData);
//         records.push(parsedMemoData);
//         if (parsedMemoData.prev_hash != 'xrp://NONE') {
//           var prevHash = parsedMemoData.prev_hash;
//           if (parsedMemoData.prev_hash.startsWith('xrp://')) {
//             prevHash = parsedMemoData.prev_hash.substring(6);
//           }
//           aqfr.nagivateRecords(prevHash, records, callback);
//         } else {
//           callback(records);
//         }
//       } else {
//         callback(records);
//       }
//     });
//   }

//   getTx(hash, callback) {
//     const request = {
//       id: this.txId++,
//       command: 'tx',
//       transaction: hash,
//       binary: false,
//     };
//     this.requestMap[request.txId.toString()] = function(data) {
//       callback(data);
//     };
//     this.connection.send(JSON.stringify(request));
//   }

//   async pay(destinationAddress, amount, memoKey, memoData, callback) {
//     const aqfr = this;
//     this.getLedgerAccountInfo(this.address, function(data) {
//       const memoRecord = {
//         timestamp: new Date().toISOString(),
//         data_json: memoData,
//         address: aqfr.address,
//       };

//       const memoKeyHex = this._api.convertStringToHex(memoKey).toUpperCase();
//       const memoDataHex = this._api.convertStringToHex(JSON.stringify(memoRecord)).toUpperCase();

//       var sequence = data.result.account_data.Sequence;

//       const txn = {
//         TransactionType: 'Payment',
//         Account: aqfr.address,
//         Destination: destinationAddress,
//         Amount: amount,
//         Fee: '20',

//         Memos: [
//           {
//             Memo: {
//               MemoType: memoKeyHex,
//               MemoData: memoDataHex,
//             },
//           },
//         ],
//         Sequence: sequence,
//       };

//       const signed = aqfr._api.sign(JSON.stringify(txn), aqfr.secret);
//       const txnObj = {
//         id: aqfr.txId++,
//         command: 'submit',
//         tx_blob: signed.signedTransaction,
//       };

//       aqfr.requestMap[txnObj.txId.toString()] = function(data) {
//         callback(sequence, data.result);
//       };
//       aqfr.connection.send(JSON.stringify(txnObj));
//     });
//   }

//   async setMemo(memoKey, memoData, callback) {
//     const aqfr = this;
//     this.getLedgerAccountInfo(this.address, function(data) {
//       var prevHash = 'NONE';
//       if (typeof data.result.account_data.Domain !== 'undefined') {
//         const domain = data.result.account_data.Domain;
//         const decodedDomain = aqfr.fromHex(domain);

//         if (decodedDomain.startsWith('xrp://')) {
//           prevHash = decodedDomain.substring(6);
//         }
//       }

//       const memoRecord = {
//         prev_hash: 'xrp://' + prevHash,
//         timestamp: new Date().toISOString(),
//         data_json: memoData,
//         address: aqfr.address,
//       };

//       const memoKeyHex = this._api.convertStringToHex(memoKey).toUpperCase();
//       const memoDataHex = this._api.convertStringToHex(JSON.stringify(memoRecord)).toUpperCase();

//       var sequence = data.result.account_data.Sequence;

//       const txn = {
//         TransactionType: 'AccountSet',
//         Account: aqfr.address,
//         Sequence: sequence,
//         Memos: [
//           {
//             Memo: {
//               MemoType: memoKeyHex,
//               MemoData: memoDataHex,
//             },
//           },
//         ],
//         Fee: '20',
//       };
//       const signed = aqfr._api.sign(JSON.stringify(txn), aqfr.secret);
//       const txnObj = {
//         id: aqfr.txId++,
//         command: 'submit',
//         tx_blob: signed.signedTransaction,
//       };

//       aqfr.requestMap[txnObj.txId.toString()] = function(data) {
//         callback(sequence, data.result);
//       };
//       aqfr.connection.send(JSON.stringify(txnObj));
//     });
//   }

//   async connectWs(options, callback) {
//     if (this.connection == null) {
//       this.connection = new WebSocket(
//         typeof options.server === 'undefined'
//           ? 'wss://s.altnet.rippletest.net:51233'
//           : 'wss://s1.ripple.com/'
//       );
//       this._api = new RippleAPI();
//       this.connection.onopen = function open() {
//         console.log('connected');
//         callback('connected');
//       };

//       this.connection.onclose = function close() {
//         console.log('disconnected');
//       };

//       const aqfr = this;
//       this.connection.onmessage = function incoming(data) {
//         const response = JSON.parse(data.data);
//         if (typeof response.txId !== 'undefined' && response.txId != 'Subscribe to Central') {
//           if (typeof aqfr.requestMap[JSON.parse(data.data).txId.toString()] !== 'undefined') {
//             const callback = aqfr.requestMap[JSON.parse(data.data).txId.toString()];
//             delete aqfr.requestMap[JSON.parse(data.data).txId.toString()];
//             callback(JSON.parse(data.data));
//           }
//         } else {
//           aqfr.subscribeCallback(data.data);
//         }
//       };
//     } else {
//       callback('connected');
//     }
//   }

//   getAccountTxns(address, callback) {
//     const request = {
//       id: this.txId++,
//       command: 'account_tx',
//       account: this.address,
//       ledger_index_min: -1,
//       ledger_index_max: -1,
//       binary: false,
//       limit: 2,
//       forward: false,
//     };
//     this.requestMap[request.txId.toString()] = callback;
//     this.connection.send(JSON.stringify(request));
//   }

//   toHex(s) {
//     // utf8 to latin1
//     var s = unescape(encodeURIComponent(s));
//     var h = '';
//     for (var i = 0; i < s.length; i++) {
//       h += s.charCodeAt(i).toString(16);
//     }
//     return h;
//   }

//   fromHex(h) {
//     var s = '';
//     for (var i = 0; i < h.length; i += 2) {
//       s += String.fromCharCode(parseInt(h.substr(i, 2), 16));
//     }
//     return decodeURIComponent(escape(s));
//   }

//   getMode() {
//     return this.mode;
//   }

//   async getFee() {
//     if (this._api.isConnected()) {
//       return this._api.getFee();
//     } else {
//       return null;
//     }
//   }

//   async initAccount(options, callback) {
//     if (this.address == null) {
//       if (typeof options.address === 'undefined') {
//         let data = await this.initTestAccount();
//         this.address = data.data.account.address;
//         this.secret = data.data.account.secret;
//         callback(this.address);
//       } else {
//         this.address = options.address;
//         this.secret = options.secret;
//         callback(this.address);
//       }
//     } else {
//       callback(this.address);
//     }
//   }

//   async initTestAccount() {
//     let result = await axios.post('https://faucet.altnet.rippletest.net/accounts');
//     return await result;
//   }

//   getAddress() {
//     return this.address;
//   }
// }

// export { MessagingAPI };
