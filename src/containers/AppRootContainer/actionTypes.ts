import { LedgerAccount, ErrorResponse } from 'src/entities';

export enum actionTypes {
  CLEAR_ACCOUNT = 'CLEAR_ACCOUNT',
  SOCKET_CONNECTED = 'SOCKET_CONNECTED',
  SOCKET_DISCONNECTED = 'SOCKET_DISCONNECTED',
  SOCKET_EMIT_MESSAGE = 'SOCKET_EMIT_MESSAGE',
  SET_ACCOUNT_CREDENTIALS_START = 'SET_ACCOUNT_CREDENTIALS_START',
  SET_ACCOUNT_CREDENTIALS_DONE = 'SET_ACCOUNT_CREDENTIALS_DONE',
  SET_ACCOUNT_CREDENTIALS_FAIL = 'SET_ACCOUNT_CREDENTIALS_FAIL',
}

export interface ClearAccount {
  type: 'CLEAR_ACCOUNT';
}

export interface SocketConnected {
  type: 'SOCKET_CONNECTED';
  isConnected: boolean;
}

export interface SocketDisconnected {
  type: 'SOCKET_DISCONNECTED';
  isConnected: boolean;
}

export interface SocketEmitMessage {
  type: 'SOCKET_EMIT_MESSAGE';
  data: any;
}

export interface SetAccountCredentialsStart {
  type: 'SET_ACCOUNT_CREDENTIALS_START';
}

export interface SetAccountCredentialsDone {
  type: 'SET_ACCOUNT_CREDENTIALS_DONE';
  account: LedgerAccount;
}

export interface SetAccountCredentialsFail {
  type: 'SET_ACCOUNT_CREDENTIALS_FAIL';
  error: ErrorResponse;
}
