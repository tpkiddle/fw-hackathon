import * as React from 'react';
import { Steps } from 'antd';
import { Card } from '../Card';

import './styles.css';

const { Step } = Steps;

interface ProgressCardProps {
  activeStep: number;
}

export class ProgressCard extends React.Component<ProgressCardProps> {
  constructor(props: ProgressCardProps) {
    super(props);
  }

  static defaultProps = {
    selected: false,
  };

  render() {
    return (
      <React.Fragment>
        <div className={`ProgressCard`}>
          <Card>
            <Steps direction="vertical" current={this.props.activeStep}>
              <Step title="Buy XRP at Source (Bitso)" />
              <Step title="Transfer to Destination (Bitstamp)" />
              <Step title="Sell XRP at Destination (Bitstamp)" />
              <Step title="Finalize Trade" />
            </Steps>
          </Card>
        </div>
      </React.Fragment>
    );
  }
}
