import { RippleAPI } from 'ripple-lib';
import { EventEmitter } from 'events';
import { FormattedSettings } from 'ripple-lib/dist/npm/common/types/objects';
import { TransactionOptions } from 'ripple-lib/dist/npm/ledger/transaction';

export interface MessageAPIProps {
  server: string;
}

export interface Credentials {
  address: string;
  secret: string;
}

export type Address = string;

class MessageAPI extends EventEmitter {
  constructor(props: MessageAPIProps) {
    super();
    this._api = new RippleAPI({ server: props.server });
  }

  address: string;
  secret: string;

  private _api: RippleAPI;

  setCredentials(credentials: Credentials) {
    this.secret = credentials.secret;
    this.address = credentials.address;
  }

  transactionHasMemoType(transaction: any, type: string): boolean {
    if (transaction && transaction.Memos !== undefined) {
      const memo = transaction.Memos[0];
      const memoType = this.fromHex(memo.Memo.MemoType);

      if (memoType === type) {
        return true;
      }
    }
    return false;
  }

  getMemoFromTransactionByMemoType(transaction: any, type: string): object | null {
    if (transaction && transaction.Memos !== undefined) {
      const memo = transaction.Memos[0];
      const memoType = this.fromHex(memo.Memo.MemoType);
      const memoData = this.fromHex(memo.Memo.MemoData);
      const parsedMemoData = JSON.parse(memoData);

      if (memoType === type) {
        return { ...parsedMemoData, hash: transaction.hash };
      }
    }

    return null;
  }

  /**
   * TODO (TK) - Figure out what these return types should be
   * At a guess Promise<FormattedPaymentTransaction | FormattedSettingsTransaction>
   */
  getSingleTransaction(hash: string): Promise<any> {
    const params = {
      transaction: hash,
      binary: false,
    } as TransactionOptions;

    return this._api.request('tx', { ...params });
  }

  async nagivateTransactionsByMemoType(
    prevHash: string,
    type: string,
    list?: any[]
  ): Promise<any[]> {
    const messages: any[] = list && list.length ? [...list] : [];

    const transaction = await this.getSingleTransaction(prevHash);

    if (transaction && transaction.Memos !== undefined) {
      const memo = transaction.Memos[0];
      const memoType = this.fromHex(memo.Memo.MemoType);
      const memoData = this.fromHex(memo.Memo.MemoData);
      const parsedMemoData = JSON.parse(memoData);

      if (memoType === type) {
        messages.push({ ...parsedMemoData, hash: transaction.hash });
      }

      if (parsedMemoData.prev_hash !== 'xrp://NONE') {
        let prevHash = parsedMemoData.prev_hash;

        if (parsedMemoData.prev_hash.startsWith('xrp://')) {
          prevHash = parsedMemoData.prev_hash.substring(6);
        }

        return await this.nagivateTransactionsByMemoType(prevHash, type, messages);
      }
    }
    return new Promise(resolve => resolve(messages));
  }

  async getTransactionsByMemoType(address: string, type: string): Promise<any[]> {
    const settings = await this._api.getSettings(address);
    let prevHash = 'NONE';

    if (settings.domain !== undefined) {
      const domain = settings.domain;

      if (domain.startsWith('xrp://')) {
        prevHash = domain.substring(6);
      }
    }

    if (prevHash === 'NONE') {
      return new Promise(resolve => resolve([]));
    }
    return this.nagivateTransactionsByMemoType(prevHash, type);
  }

  async getLocalTransactionsByMemoType(type: string): Promise<any[]> {
    return this.getTransactionsByMemoType(this.address, type);
  }

  async nagivateRecords(prevHash: string, messages: any[]): Promise<any[]> {
    const transaction = await this.getSingleTransaction(prevHash);

    if (transaction && transaction.Memos !== undefined) {
      const memo = transaction.Memos[0];
      const memoData = this.fromHex(memo.Memo.MemoData);
      const parsedMemoData = JSON.parse(memoData);

      messages.push({ ...parsedMemoData, hash: transaction.hash });

      if (parsedMemoData.prev_hash !== 'xrp://NONE') {
        let prevHash = parsedMemoData.prev_hash;

        if (parsedMemoData.prev_hash.startsWith('xrp://')) {
          prevHash = parsedMemoData.prev_hash.substring(6);
        }

        return this.nagivateRecords(prevHash, messages);
      }
    }

    return new Promise(resolve => messages);
  }

  async getTransactions(address: string): Promise<any[]> {
    const records: any[] = [];
    const settings = await this._api.getSettings(address);

    let prevHash = 'NONE';

    if (settings.domain !== undefined) {
      const domain = settings.domain;
      if (domain.startsWith('xrp://')) {
        prevHash = domain.substring(6);
      }
    }

    if (prevHash === 'NONE') {
      return new Promise(resolve => resolve([]));
    }

    return this.nagivateRecords(prevHash, records);
  }

  getLocalTransactions(): Promise<any[]> {
    return this.getTransactions(this.address);
  }

  // async pay(destination: string, amount: string, memoKey: string, memoData: string) {
  //   const accountInfo = await this.getLedgerAccountInfo(this.address);

  //   const memoRecord = {
  //     timestamp: new Date().toISOString(),
  //     data_json: memoData,
  //     address: this.address,
  //   };

  //   const memoKeyHex = this._api.convertStringToHex(memoKey).toUpperCase();
  //   const memoDataHex = this._api.convertStringToHex(JSON.stringify(memoRecord)).toUpperCase();

  //   const sequence = accountInfo.account_data.Sequence;

  //   const txn = {
  //     TransactionType: 'Payment',
  //     Account: this.address,
  //     Destination: destination,
  //     Amount: amount,
  //     Fee: '20',

  //     Memos: [
  //       {
  //         Memo: {
  //           MemoType: memoKeyHex,
  //           MemoData: memoDataHex,
  //         },
  //       },
  //     ],
  //     Sequence: sequence,
  //   };

  //   const signed = new RippleAPI().sign(JSON.stringify(txn), this.secret);
  //   const txnObj = {
  //     id: this._transactionId++,
  //     tx_blob: signed.signedTransaction,
  //   };

  //   return this._api.request('submit', txnObj);
  // }

  async setSettings(settings: FormattedSettings) {
    const api = new RippleAPI();
    const prepared = await this._api.prepareSettings(this.address, settings, { fee: '2' });
    const signed = api.sign(prepared.txJSON, this.secret);

    await this._api.submit(signed.signedTransaction);
    return signed.id;
  }

  async setPrivateMessage(message: string): Promise<string | null> {
    const transaction = await this.setMemo('SOCIALLY', { text: message });

    await this.setSettings({
      domain: `xrp://${transaction}`,
    });

    return transaction;
  }

  async setMemo(memoKey: string, memoData: { [key: string]: string }) {
    const settings = await this._api.getSettings(this.address);
    let prevHash = 'NONE';

    if (settings.domain !== undefined) {
      const domain = settings.domain;

      if (domain.startsWith('xrp://')) {
        prevHash = domain.substring(6);
      }
    }

    const memoRecord = {
      body: memoData,
      address: this.address,
      prev_hash: `xrp://${prevHash}`,
      timestamp: new Date().toISOString(),
    };

    const params = {
      domain: prevHash === 'NONE' ? `xrp://${prevHash}` : undefined,
      memos: [
        {
          type: memoKey,
          format: 'text/plain',
          data: JSON.stringify(memoRecord),
        },
      ],
    };

    return this.setSettings(params);
  }

  subscribeToAccounts(accounts: Address[]): Promise<any> {
    const params = {
      id: 'subscribe_to_central',
      accounts: accounts && accounts.length ? accounts : [this.address],
    };

    return this._api.request('subscribe', { ...params });
  }

  subscribeToLocalAccount(): Promise<any> {
    const params = {
      id: 'subscribe_to_local',
      command: 'subscribe',
      accounts: [this.address],
    };

    return this._api.connection.request({ ...params });
  }

  connect() {
    if (!this._api.isConnected()) {
      this._api.connect();

      this._api.on('connected', () => {
        /**
         * Emit a new event when the socket connects
         * allowing the MessageAPI consumer to react.
         */
        this.emit('connected');
      });

      this._api.on('disconnected', () => {
        /**
         * Emit a new event when the socket disconnects
         * allowing the MessageAPI consumer to react.
         */
        this.emit('disconnected');
      });

      this._api.connection.on('transaction', data => {
        /**
         * Emit a new event when the socket recieves a transaction
         *  on the local account allowing the MessageAPI consumer to react.
         */
        const isMessageTransaction = this.transactionHasMemoType(data.transaction, 'SOCIALLY');

        if (isMessageTransaction) {
          this.emit(
            'message_created',
            this.getMemoFromTransactionByMemoType(data.transaction, 'SOCIALLY')
          );
        }
      });
    }
  }

  fromHex(hex: string) {
    let result = '';

    for (let i = 0; i < hex.length; i += 2) {
      result += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
    }
    return decodeURIComponent(escape(result));
  }

  //   getAccountTxns(address, callback) {
  //     const request = {
  //       id: this._transactionId++,
  //       command: "account_tx",
  //       account: this.address,
  //       ledger_index_min: -1,
  //       ledger_index_max: -1,
  //       binary: false,
  //       limit: 2,
  //       forward: false
  //     };
  //     this.requestMap[request._transactionId.toString()] = callback;
  //     this.connection.send(JSON.stringify(request));
  //   }

  //   async initAccount(options, callback) {
  //     if (this.address == null) {
  //       if (typeof options.address === "undefined") {
  //         let data = await this.initTestAccount();
  //         this.address = data.data.account.address;
  //         this.secret = data.data.account.secret;
  //         callback(this.address);
  //       } else {
  //         this.address = options.address;
  //         this.secret = options.secret;
  //         callback(this.address);
  //       }
  //     } else {
  //       callback(this.address);
  //   }
  // }

  // getAddress() {
  //   return this.address;
  // }
}

export { MessageAPI };

// export const initTestAccount = async () => {
//   let result = await axios.post(
//     "https://faucet.altnet.rippletest.net/accounts"
//   );
//   return await result;
// };
