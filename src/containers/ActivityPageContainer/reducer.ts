import { actionTypes } from './actionTypes';
import { RegisteredAction } from 'src/actions';
import { FetchedData } from 'src/entities';
import { createFetchedData } from 'src/utils/misc';

export interface ActivityPageContainerState {
  messages: FetchedData<any[]>;
}

export const initialState: ActivityPageContainerState = {
  messages: createFetchedData(),
};

export function reducer(
  state: ActivityPageContainerState = initialState,
  action: RegisteredAction
) {
  switch (action.type) {
    case actionTypes.GET_MESSAGES_START:
      return {
        ...state,
        messages: {
          isLoading: true,
          error: null,
          data: null,
        },
      };
    case actionTypes.GET_MESSAGES_DONE:
      return {
        ...state,
        messages: {
          isLoading: false,
          error: null,
          data: action.messages,
        },
      };
    case actionTypes.GET_MESSAGES_FAIL:
      return {
        ...state,
        messages: {
          isLoading: false,
          error: action.error,
          data: null,
        },
      };

    default:
      return state;
  }
}
