import * as React from 'react';
import { LoaderCentered } from '../LoaderCentered';

import './styles.css';
import { GhostButton } from '../GhostButton';

interface TradeActivityCardProps {
  selected?: boolean;
  sourceCurrency: string;
  destinationCurrency: string;
  sourceAmount: string;
  destinationAmount: string;
  sourceExchange: string;
  destinationExchange: string;
  tradedAmount: string;
  profitAmount: string;
}

export class TradeActivityCard extends React.Component<TradeActivityCardProps, any> {
  constructor(props: TradeActivityCardProps) {
    super(props);

    this.state = {
      showMore: false,
    };

    this.handleClick = this.handleClick.bind(this);
  }

  static defaultProps = {
    selected: false,
  };

  renderChart() {
    return (
      <div className="chart-wrapper">
        <img src="images/faux-chart.svg" />
      </div>
    );
  }

  handleClick() {
    this.setState({ showMore: !this.state.showMore });
  }

  renderButton() {
    if (this.state.showMore) {
      return <GhostButton onClick={this.handleClick}>Hide Details</GhostButton>;
    }

    return <GhostButton onClick={this.handleClick}>View Details</GhostButton>;
  }

  render() {
    const {
      selected,
      sourceCurrency,
      destinationCurrency,
      sourceAmount,
      destinationAmount,
      sourceExchange,
      destinationExchange,
      tradedAmount,
      profitAmount,
    } = this.props;

    return (
      <React.Fragment>
        <div className={`TradeActivityCard ${selected ? 'selected' : 'unselected'}`}>
          {false ? <LoaderCentered /> : null}
          <div className="date-bar">5/15/20 10:54:14</div>
          <div className="top-content">
            <div className="currency-info">
              <div className="currency">{sourceCurrency}</div>
              <div className="rate">{sourceAmount}</div>
              <div className="exchange">{sourceExchange}</div>
            </div>
            <div className="cost-saving">
              <img className="arrow-image" src="images/unselected-arrow.svg" />
            </div>
            <div className="currency-info">
              <div className="currency">{destinationCurrency}</div>
              <div className="rate">{destinationAmount}</div>
              <div className="exchange">{destinationExchange}</div>
            </div>
          </div>
          <div className="bottom-content">
            <div className="currency-info">
              <div className="exchange">Traded (in XRP)</div>
              <div className="pre-trade-amount">{tradedAmount}</div>
            </div>
            <div className="currency-info">
              <div className="exchange">Profit (in XRP)</div>
              <div className="pre-trade-amount">{profitAmount}</div>
            </div>
          </div>
          {this.state.showMore ? (
            <div className="additional-content-wrapper">{this.renderChart()} </div>
          ) : null}
          <div className="details-action">{this.renderButton()}</div>
        </div>
      </React.Fragment>
    );
  }
}
