import { connect } from 'react-redux';
import { StartPage, StartPageProps } from './page';
import { RootState } from 'src/rootReducer';
import { performGetTestLedgerAccount } from '../AppRootContainer/actions';

const mapStateToProps = (state: RootState): Partial<StartPageProps> => {
  return {
    account: state.app.account,
  };
};

const mapDispatchToProps = {
  onSetAccountCredentials: performGetTestLedgerAccount,
};

export const StartPageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(StartPage);
