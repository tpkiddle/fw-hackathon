import { actionTypes } from './actionTypes';
import { RegisteredAction } from 'src/actions';
import { FetchedData } from 'src/entities';
import { createFetchedData } from 'src/utils/misc';

export interface BalancesPageContainerState {
  balances: FetchedData<any>;
}

export const initialState: BalancesPageContainerState = {
  balances: createFetchedData(),
};

export function reducer(
  state: BalancesPageContainerState = initialState,
  action: RegisteredAction
) {
  switch (action.type) {
    case actionTypes.GET_BALANCES_START:
      return {
        ...state,
        balances: {
          isLoading: true,
          error: null,
          data: null,
        },
      };
    case actionTypes.GET_BALANCES_DONE:
      return {
        ...state,
        balances: {
          isLoading: false,
          error: null,
          data: action.balances,
        },
      };
    case actionTypes.GET_BALANCES_FAIL:
      return {
        ...state,
        balances: {
          isLoading: false,
          error: action.error,
          data: null,
        },
      };
    default:
      return state;
  }
}
