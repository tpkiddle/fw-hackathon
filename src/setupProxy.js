const proxy = require('http-proxy-middleware');

const proxyOptions = {
  target: 'http://5a7c7c6c.ngrok.io/',
  xfwd: true,
  secure: false,
  changeOrigin: true,
  logLevel: 'silent',
};

module.exports = function(app) {
  app.use(proxy(['/v1'], proxyOptions));
};
