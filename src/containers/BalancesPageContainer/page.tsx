import * as React from 'react';
import { FetchedData } from 'src/entities';
import { TopBar } from '../../components/TopBar';
import './styles.css';

import { LoaderCentered } from 'src/components/LoaderCentered';
import { Heading } from 'src/components/Heading';

export interface BalancesPageProps {
  balances: FetchedData<any[]>;
  onGetBalances: () => void;
}

export interface BalancesPageState {}

export class BalancesPage extends React.Component<BalancesPageProps, BalancesPageState> {
  constructor(props: BalancesPageProps) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
    this.props.onGetBalances();
  }

  renderActivityList() {
    const { balances } = this.props;

    if (!balances.data || balances.error) {
      return 'There has been an error!';
    }

    return Object.keys(balances.data).map(key => {
      return (
        <div className="balance-card" key={key}>
          <div className="card-content">
            <div className="exchange-name">{key}</div>
            <div>
              {Object.keys(balances.data![key]).map(currency => {
                return (
                  <div key={currency}>
                    <span className="currency-name">{currency}</span>
                    <span className="currency-balance">{balances.data![key][currency]}</span>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      );
    });
  }

  renderBody() {
    const { balances } = this.props;
    console.log('balances', balances);
    if (balances.isLoading) {
      return <LoaderCentered />;
    }

    return this.renderActivityList();
  }

  render() {
    return (
      <div className="BalancesPage">
        <TopBar />
        <div className="content">
          <div className="content-body">
            <Heading>Exhange Account Balances</Heading>
            <div className="content-wrapper">{this.renderBody()}</div>
          </div>
        </div>
      </div>
    );
  }
}
