import * as React from 'react';

import './styles.css';
import { Link } from 'react-router-dom';
import { Menu } from '../Menu';

interface TopBarProps {
  className?: string;
}

export class TopBar extends React.Component<TopBarProps> {
  constructor(props: TopBarProps) {
    super(props);
  }

  renderMenu() {
    return <Menu />;
  }

  render() {
    const { className } = this.props;

    return (
      <div className={`TopBar ${className}`}>
        <div className="brand-name">
          <Link to="/">
            <img src="images/logo.svg" className="logo" />
          </Link>
        </div>
        <div className="content-right">{this.renderMenu()}</div>
      </div>
    );
  }
}
