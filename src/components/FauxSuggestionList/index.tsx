import * as React from 'react';
import { Suggestion } from '../Suggestion';

interface FauxSuggestionListProps {}

export class FauxSuggestionList extends React.Component<FauxSuggestionListProps> {
  constructor(props: FauxSuggestionListProps) {
    super(props);
  }
  render() {
    return (
      <div className={`FauxSuggestionList`}>
        <Suggestion
          costSavingLabel="XRP Minor Difference"
          sourceExchange="Bitstamp"
          sourceCurrency="EUR"
          sourceAmount="1.1221"
          destinationExchange="Bitstamp"
          destinationCurrency="USD"
          destinationAmount="1.1221"
        />
        <Suggestion
          costSavingLabel="XRP Minor Difference"
          sourceExchange="Coinfield"
          sourceCurrency="GBP"
          sourceAmount="1.2512"
          destinationExchange="Bitstamp"
          destinationCurrency="USD"
          destinationAmount="1.2512"
        />
        <Suggestion
          costSavingLabel="Minor Difference"
          sourceExchange="Bitstamp"
          sourceCurrency="USD"
          sourceAmount="19.4014"
          destinationExchange="Bitso"
          destinationCurrency="MXN"
          destinationAmount="19.4321"
        />
        <Suggestion
          costSavingLabel="Minor Difference"
          sourceExchange="Bitstamp"
          sourceCurrency="USD"
          sourceAmount="19.4014"
          destinationExchange="Bitso"
          destinationCurrency="MXN"
          destinationAmount="19.4321"
        />
      </div>
    );
  }
}
