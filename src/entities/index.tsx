export interface FetchedData<T> {
  isLoading: boolean;
  error: RippledErrorResponse | ErrorResponse | null;
  data: T | null;
}

export interface SuccessResponse<T> {
  data: T;
  success: true;
}

export interface ErrorResponse {
  success: false;
  code?: number;
  message: string;
}

export interface RippledErrorResponse {
  name: string;
  message: string;
}

export type Response<T> = SuccessResponse<T> | ErrorResponse;

export interface User {
  id: string;
  email: string;
  name: string;
  xrpl_public_key: string;
  created: string;
  updated: string;
}

export interface Message {
  id: string;
  owner_id: string;
  created: string;
  updated: string;
  message: string;
  recepient_id: string;
}

export interface LedgerAccount {
  secret: string;
  address: string;
}
