import { connect } from 'react-redux';
import { RootState } from 'src/rootReducer';
import { AppRoot, AppRootProps } from './AppRoot';

const mapStateToProps = (state: RootState): Partial<AppRootProps> => {
  return {
    account: state.app.account,
    isConnected: state.app.isConnected,
  };
};

const mapDispatchToProps = {};

export const AppRootContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(AppRoot);
