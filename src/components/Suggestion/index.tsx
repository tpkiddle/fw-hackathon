import * as React from 'react';

import './styles.css';
import { LoaderCentered } from '../LoaderCentered';
import { Card } from '../Card';
import { Button } from '../Button';

interface SuggestionProps {
  selected?: boolean;
  costSavingLabel: string;
  sourceCurrency: string;
  destinationCurrency: string;
  sourceAmount: string;
  destinationAmount: string;
  sourceExchange: string;
  destinationExchange: string;
  onClick?: (e: React.MouseEvent<HTMLButtonElement>) => void;
}

export class Suggestion extends React.Component<SuggestionProps, any> {
  constructor(props: SuggestionProps) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  static defaultProps = {
    selected: false,
  };

  handleChange(e: React.MouseEvent<HTMLButtonElement>) {
    const { onClick } = this.props;
    onClick && onClick(e);
  }

  renderChart() {
    return (
      <div className="chart-wrapper">
        <img src="images/faux-chart.svg" />
      </div>
    );
  }

  render() {
    const {
      selected,
      costSavingLabel,
      sourceCurrency,
      destinationCurrency,
      sourceAmount,
      destinationAmount,
      sourceExchange,
      destinationExchange,
    } = this.props;

    return (
      <React.Fragment>
        <div className={`Suggestion ${selected ? 'selected' : 'unselected'}`}>
          <Card additionalContent={this.renderChart()}>
            {false ? <LoaderCentered /> : null}
            <div className="top-content">
              <div className="currency-info">
                <div className="currency">{sourceCurrency}</div>
                <div className="rate">{sourceAmount}</div>
                <div className="exchange">{sourceExchange}</div>
              </div>
              <div className="cost-saving">
                {selected ? (
                  <img className="arrow-image" src="images/arrow.svg" />
                ) : (
                  <img className="arrow-image" src="images/unselected-arrow.svg" />
                )}
                <div className="cost-saving-description">{costSavingLabel}</div>
              </div>
              <div className="currency-info">
                <div className="currency">{destinationCurrency}</div>
                <div className="rate">{destinationAmount}</div>
                <div className="exchange">{destinationExchange}</div>
              </div>
            </div>
            {selected ? (
              <div className="bottom-content">
                <Button onClick={this.handleChange}>Trade</Button>
              </div>
            ) : null}
          </Card>
        </div>
      </React.Fragment>
    );
  }
}
