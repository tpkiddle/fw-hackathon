import * as React from 'react';

import './styles.css';

interface TextAreaProps {
  placeholder: string;
  onChange?: (e: React.ChangeEvent<HTMLTextAreaElement>) => void;
}

export class TextArea extends React.Component<TextAreaProps> {
  constructor(props: TextAreaProps) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e: React.ChangeEvent<HTMLTextAreaElement>) {
    const { onChange } = this.props;
    onChange && onChange(e);
  }

  render() {
    return (
      <div className="TextArea">
        <textarea onChange={this.handleChange} {...this.props} />
      </div>
    );
  }
}
