import { actionTypes } from './actionTypes';
import { RegisteredAction } from 'src/actions';
import { Transform, createTransform } from 'redux-persist';
import { FetchedData, LedgerAccount } from 'src/entities';
import { createFetchedData } from 'src/utils/misc';

export interface AppRootContainerState {
  account: FetchedData<LedgerAccount>;
  isConnected: boolean;
}

export const initialState: AppRootContainerState = {
  account: createFetchedData(),
  isConnected: false,
};

export const persistor: Transform<any, any> = createTransform(({}) => ({}), ({}) => ({}), {
  whitelist: ['app'],
});

export function reducer(state: AppRootContainerState = initialState, action: RegisteredAction) {
  switch (action.type) {
    case actionTypes.CLEAR_ACCOUNT:
      return {
        ...state,
        account: {
          isLoading: false,
          error: null,
          data: null,
        },
      };
    case actionTypes.SOCKET_CONNECTED:
      return {
        ...state,
        isConnected: action.isConnected,
      };
    case actionTypes.SOCKET_DISCONNECTED:
      return {
        ...state,
        isConnected: action.isConnected,
        account: {
          isLoading: false,
          error: null,
          data: null,
        },
      };
    case actionTypes.SOCKET_EMIT_MESSAGE:
      return {
        ...state,
        data: action.data,
      };
    case actionTypes.SET_ACCOUNT_CREDENTIALS_START:
      return {
        ...state,
        account: {
          isLoading: true,
          error: null,
          data: null,
        },
      };
    case actionTypes.SET_ACCOUNT_CREDENTIALS_DONE:
      return {
        ...state,
        account: {
          isLoading: false,
          error: null,
          data: action.account,
        },
      };
    case actionTypes.SET_ACCOUNT_CREDENTIALS_FAIL:
      return {
        ...state,
        account: {
          isLoading: false,
          error: action.error,
          data: null,
        },
      };
    default:
      return state;
  }
}
