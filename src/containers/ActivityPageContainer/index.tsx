import { connect } from 'react-redux';
import { ActivityPage, ActivityPageProps } from './page';
import { RootState } from 'src/rootReducer';
import { clearAccount, performGetTestLedgerAccount } from '../AppRootContainer/actions';
import { performGetMessages } from './actions';

const mapStateToProps = (state: RootState): Partial<ActivityPageProps> => {
  return {
    account: state.app.account,
    isConnected: state.app.isConnected,
    messages: state.activityPage.messages,
  };
};

const mapDispatchToProps = {
  onClearAccount: clearAccount,
  onGetMessages: performGetMessages,
  onSetAccountCredentials: performGetTestLedgerAccount,
};

export const ActivityPageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ActivityPage);
