import { RippledErrorResponse } from 'src/entities';

export enum actionTypes {
  GET_MESSAGES_START = 'GET_MESSAGES_START',
  GET_MESSAGES_DONE = 'GET_MESSAGES_DONE',
  GET_MESSAGES_FAIL = 'GET_MESSAGES_FAIL',
  CREATE_MESSAGE_START = 'CREATE_MESSAGE_START',
  CREATE_MESSAGE_DONE = 'CREATE_MESSAGE_DONE',
  CREATE_MESSAGE_FAIL = 'CREATE_MESSAGE_FAIL',
  'PATCH_MESSAGE_DONE' = 'PATCH_MESSAGE_DONE',
}

export interface GetMessagesStart {
  type: 'GET_MESSAGES_START';
}

export interface GetMessagesDone {
  type: 'GET_MESSAGES_DONE';
  messages: any[];
}

export interface GetMessagesFail {
  type: 'GET_MESSAGES_FAIL';
  error: RippledErrorResponse;
}

export interface CreateMessageStart {
  type: 'CREATE_MESSAGE_START';
}

export interface CreateMessageDone {
  type: 'CREATE_MESSAGE_DONE';
  message: boolean;
}

export interface CreateMessageFail {
  type: 'CREATE_MESSAGE_FAIL';
  error: RippledErrorResponse;
}

export interface PatchMessageDone {
  type: 'PATCH_MESSAGE_DONE';
  message: boolean;
}
