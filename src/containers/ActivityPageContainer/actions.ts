import { GetMessagesStart, GetMessagesDone, GetMessagesFail } from './actionTypes';
import { RegisteredAction } from 'src/actions';
import { Dispatch } from 'react';
import { RippledErrorResponse } from 'src/entities';
import { normalizeRippledError } from 'src/api';

export function getMessagesStart(): GetMessagesStart {
  return { type: 'GET_MESSAGES_START' };
}

export function getMessagesDone(messages: any[]): GetMessagesDone {
  return { type: 'GET_MESSAGES_DONE', messages };
}

export function getMessagesFail(error: RippledErrorResponse): GetMessagesFail {
  return { type: 'GET_MESSAGES_FAIL', error };
}

export function performGetMessages() {
  return (dispatch: Dispatch<RegisteredAction>, getState: any, api: any) => {
    dispatch(getMessagesStart());

    api
      .getLocalTransactionsByMemoType('SOCIALLY')
      .then((response: any) => {
        dispatch(getMessagesDone(response));
      })
      .catch((error: any) => {
        dispatch(getMessagesFail(normalizeRippledError(error)));
      });
  };
}
