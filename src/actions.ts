import * as AppRootContainer from './containers/AppRootContainer/actionTypes';
import * as ActivityPageContainer from './containers/ActivityPageContainer/actionTypes';
import * as SuggestionsPageContainer from './containers/SuggestionsPageContainer/actionTypes';
import * as BalancesPageContainer from './containers/BalancesPageContainer/actionTypes';

export type RegisteredAction =
  | AppRootContainer.ClearAccount
  | AppRootContainer.SocketConnected
  | AppRootContainer.SocketDisconnected
  | AppRootContainer.SocketEmitMessage
  | AppRootContainer.SetAccountCredentialsStart
  | AppRootContainer.SetAccountCredentialsDone
  | AppRootContainer.SetAccountCredentialsFail
  | ActivityPageContainer.GetMessagesStart
  | ActivityPageContainer.GetMessagesDone
  | ActivityPageContainer.GetMessagesFail
  | BalancesPageContainer.GetBalancesStart
  | BalancesPageContainer.GetBalancesDone
  | BalancesPageContainer.GetBalancesFail
  | SuggestionsPageContainer.GetRatesStart
  | SuggestionsPageContainer.GetRatesDone
  | SuggestionsPageContainer.GetRatesFail
  | SuggestionsPageContainer.ExecuteStart
  | SuggestionsPageContainer.ExecuteDone
  | SuggestionsPageContainer.ExecuteFail
  | SuggestionsPageContainer.GetTradeStart
  | SuggestionsPageContainer.GetTradeDone
  | SuggestionsPageContainer.GetTradeFail
  | SuggestionsPageContainer.ClearTrade;
