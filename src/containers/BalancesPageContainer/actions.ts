import { GetBalancesStart, GetBalancesDone, GetBalancesFail } from './actionTypes';
import { RegisteredAction } from 'src/actions';
import { Dispatch } from 'react';
import { ErrorResponse } from 'src/entities';
import { getBalances } from 'src/api';

export function getBalancesStart(): GetBalancesStart {
  return { type: 'GET_BALANCES_START' };
}

export function getBalancesDone(balances: any): GetBalancesDone {
  return { type: 'GET_BALANCES_DONE', balances };
}

export function getBalancesFail(error: ErrorResponse): GetBalancesFail {
  return { type: 'GET_BALANCES_FAIL', error };
}

export function performGetBalances() {
  return (dispatch: Dispatch<RegisteredAction>) => {
    dispatch(getBalancesStart());

    getBalances()
      .then((response: any) => {
        dispatch(getBalancesDone(response.data));
      })
      .catch((error: any) => {
        dispatch(getBalancesFail(error));
      });
  };
}
