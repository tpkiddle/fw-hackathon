import { ErrorResponse } from 'src/entities';

export enum actionTypes {
  GET_BALANCES_START = 'GET_BALANCES_START',
  GET_BALANCES_DONE = 'GET_BALANCES_DONE',
  GET_BALANCES_FAIL = 'GET_BALANCES_FAIL',
}

export interface GetBalancesStart {
  type: 'GET_BALANCES_START';
}

export interface GetBalancesDone {
  type: 'GET_BALANCES_DONE';
  balances: any;
}

export interface GetBalancesFail {
  type: 'GET_BALANCES_FAIL';
  error: ErrorResponse;
}
