import { combineReducers } from 'redux';

import {
  AppRootContainerState,
  initialState as appRootContainerInitalState,
  reducer as appRootContainerReducer,
} from './containers/AppRootContainer/reducer';

import {
  ActivityPageContainerState,
  initialState as activityPageContainerInitalState,
  reducer as activityPageContainerReducer,
} from './containers/ActivityPageContainer/reducer';

import {
  SuggestionsPageContainerState,
  initialState as suggestionsPageContainerInitalState,
  reducer as suggestionsPageContainerReducer,
} from './containers/SuggestionsPageContainer/reducer';

import {
  BalancesPageContainerState,
  initialState as balancesPageContainerInitalState,
  reducer as balancesPageContainerReducer,
} from './containers/BalancesPageContainer/reducer';

import { RegisteredAction } from './actions';

export interface RootState {
  app: AppRootContainerState;
  suggestionsPage: SuggestionsPageContainerState;
  balancesPage: BalancesPageContainerState;
  activityPage: ActivityPageContainerState;
}

export function createInitialState(): RootState {
  return {
    app: appRootContainerInitalState,
    suggestionsPage: suggestionsPageContainerInitalState,
    balancesPage: balancesPageContainerInitalState,
    activityPage: activityPageContainerInitalState,
  };
}

const combinedReducers = combineReducers<RootState | void>({
  app: appRootContainerReducer,
  suggestionsPage: suggestionsPageContainerReducer,
  balancesPage: balancesPageContainerReducer,
  activityPage: activityPageContainerReducer,
});

export function rootReducer(state: RootState | void, action: RegisteredAction) {
  return combinedReducers(state, action);
}
