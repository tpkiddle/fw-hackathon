import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { StartPage } from '../page';
import { createFetchedData } from 'src/utils/misc';

const BASE_PROPS = {
  onSetAccountCredentials: jest.fn(),
  account: createFetchedData(),
};

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<StartPage {...BASE_PROPS} />, div);
  ReactDOM.unmountComponentAtNode(div);
});
