import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { AppRoot } from '../AppRoot';
import { createFetchedData } from 'src/utils/misc';

const BASE_PROPS = {
  account: createFetchedData(),
  isConnected: false,
};

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<AppRoot {...BASE_PROPS} />, div);
  ReactDOM.unmountComponentAtNode(div);
});
