import * as React from 'react';
import { FetchedData, LedgerAccount } from 'src/entities';
import { TopBar } from '../../components/TopBar';
import './styles.css';

import { UserProfitsCard } from 'src/components/UserProfitsCard';
import { TradeActivityCard } from 'src/components/TradeActivityCard';

export interface ActivityPageProps {
  account: FetchedData<LedgerAccount>;
  isConnected: boolean;
  messages: FetchedData<any[]>;
  message: FetchedData<any>;
  onClearAccount: () => void;
  onGetMessages: () => void;
  onCreateMessage: (message: string) => void;
  onSetAccountCredentials: () => void;
}

export interface ActivityPageState {
  showTradeForm: boolean;
  tradeInFlight: boolean;
  tradeComplete: boolean;
}

export class ActivityPage extends React.Component<ActivityPageProps, ActivityPageState> {
  constructor(props: ActivityPageProps) {
    super(props);

    this.state = {
      showTradeForm: false,
      tradeInFlight: false,
      tradeComplete: true,
    };

    this.handleClickTrade = this.handleClickTrade.bind(this);
    this.handleClickCancel = this.handleClickCancel.bind(this);
    this.handleClickExecute = this.handleClickExecute.bind(this);
  }

  componentDidMount() {}

  handleClickTrade(e: React.FormEvent) {
    e.preventDefault();

    this.setState({ showTradeForm: true });
  }

  handleClickCancel(e: React.FormEvent) {
    e.preventDefault();

    this.setState({ showTradeForm: false });
  }

  handleClickExecute(value: string) {
    this.setState({ showTradeForm: false, tradeInFlight: true });
  }

  renderActivityList() {
    return (
      <React.Fragment>
        <TradeActivityCard
          selected={false}
          sourceExchange="Bitstamp"
          sourceCurrency="USD"
          sourceAmount="19.4014"
          destinationExchange="Bitso"
          destinationCurrency="MXN"
          destinationAmount="19.4321"
          tradedAmount="1300"
          profitAmount="12"
        />
      </React.Fragment>
    );
  }

  render() {
    return (
      <div className="ActivityPage">
        <TopBar />
        <UserProfitsCard />
        <div className="content">
          <div className="content-body">{this.renderActivityList()}</div>
        </div>
      </div>
    );
  }
}
