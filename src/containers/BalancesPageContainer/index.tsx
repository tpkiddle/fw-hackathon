import { connect } from 'react-redux';
import { BalancesPage, BalancesPageProps } from './page';
import { RootState } from 'src/rootReducer';
import { performGetBalances } from './actions';

const mapStateToProps = (state: RootState): Partial<BalancesPageProps> => {
  return {
    balances: state.balancesPage.balances,
  };
};

const mapDispatchToProps = {
  onGetBalances: performGetBalances,
};

export const BalancesPageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(BalancesPage);
