import {
  ClearAccount,
  SocketConnected,
  SocketDisconnected,
  SocketEmitMessage,
  SetAccountCredentialsStart,
  SetAccountCredentialsDone,
  SetAccountCredentialsFail,
} from './actionTypes';
import { LedgerAccount, ErrorResponse } from 'src/entities';
import { RegisteredAction } from 'src/actions';
import { getTestAccountCredentials } from 'src/api';
import { Dispatch } from 'react';

export function clearAccount(): ClearAccount {
  return { type: 'CLEAR_ACCOUNT' };
}

export function socketConnected(): SocketConnected {
  return { type: 'SOCKET_CONNECTED', isConnected: true };
}

export function socketDisconnected(): SocketDisconnected {
  return { type: 'SOCKET_DISCONNECTED', isConnected: false };
}

export function socketEmitMessage(data: any): SocketEmitMessage {
  return { type: 'SOCKET_EMIT_MESSAGE', data };
}

export function setAccountCredentialsActionStart(): SetAccountCredentialsStart {
  return { type: 'SET_ACCOUNT_CREDENTIALS_START' };
}

export function setAccountCredentialsActionDone(account: LedgerAccount): SetAccountCredentialsDone {
  return { type: 'SET_ACCOUNT_CREDENTIALS_DONE', account };
}

export function setAccountCredentialsActionFail(error: ErrorResponse): SetAccountCredentialsFail {
  return { type: 'SET_ACCOUNT_CREDENTIALS_FAIL', error };
}

export function performGetTestLedgerAccount(credentials: { address: string; secret: string }) {
  return async (dispatch: Dispatch<RegisteredAction>, api: any) => {
    dispatch(setAccountCredentialsActionStart());
    if (credentials.address && credentials.secret) {
      dispatch(
        setAccountCredentialsActionDone({
          address: credentials.address,
          secret: credentials.secret,
        })
      );
      return;
    }

    const response = await getTestAccountCredentials();

    if (!response.success) {
      dispatch(setAccountCredentialsActionFail(response));
      return;
    }

    dispatch(setAccountCredentialsActionDone(response.data));
  };
}
