import * as React from 'react';

import './styles.css';

interface TextFieldProps {
  type?: 'text' | 'password';
  className?: string;
  placeholder?: string;
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
}

export class TextField extends React.Component<TextFieldProps> {
  constructor(props: TextFieldProps) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e: React.ChangeEvent<HTMLInputElement>) {
    const { onChange } = this.props;
    onChange && onChange(e);
  }

  render() {
    const { type, className } = this.props;

    return (
      <div className={`TextField ${className}`}>
        <input {...this.props} type={type ? type : 'text'} onChange={this.handleChange} />
      </div>
    );
  }
}
