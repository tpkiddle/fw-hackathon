import * as React from 'react';

import './styles.css';

interface RouteNotFoundProps {}

export class RouteNotFound extends React.Component<RouteNotFoundProps> {
  render() {
    return (
      <div className="RouteNotFound">
        <h1>Uh Oh - Looks like you're lost.</h1>
      </div>
    );
  }
}
