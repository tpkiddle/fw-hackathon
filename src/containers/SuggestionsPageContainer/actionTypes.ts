import { ErrorResponse } from 'src/entities';

export enum actionTypes {
  GET_RATES_START = 'GET_RATES_START',
  GET_RATES_DONE = 'GET_RATES_DONE',
  GET_RATES_FAIL = 'GET_RATES_FAIL',
  EXECUTE_START = 'EXECUTE_START',
  EXECUTE_DONE = 'EXECUTE_DONE',
  EXECUTE_FAIL = 'EXECUTE_FAIL',
  GET_TRADE_START = 'GET_TRADE_START',
  GET_TRADE_DONE = 'GET_TRADE_DONE',
  GET_TRADE_FAIL = 'GET_TRADE_FAIL',
  CLEAR_TRADE = 'CLEAR_TRADE',
}

export interface GetRatesStart {
  type: 'GET_RATES_START';
}

export interface GetRatesDone {
  type: 'GET_RATES_DONE';
  rates: any;
}

export interface GetRatesFail {
  type: 'GET_RATES_FAIL';
  error: ErrorResponse;
}

export interface ExecuteStart {
  type: 'EXECUTE_START';
}

export interface ExecuteDone {
  type: 'EXECUTE_DONE';
  trade: any;
}

export interface ExecuteFail {
  type: 'EXECUTE_FAIL';
  error: ErrorResponse;
}

export interface GetTradeStart {
  type: 'GET_TRADE_START';
}

export interface GetTradeDone {
  type: 'GET_TRADE_DONE';
  tradeCheck: any;
}

export interface GetTradeFail {
  type: 'GET_TRADE_FAIL';
  error: ErrorResponse;
}

export interface ClearTrade {
  type: 'CLEAR_TRADE';
  data: any;
}
