import * as React from 'react';
import { Icon } from 'antd';

import './styles.css';
import { Link } from 'react-router-dom';

interface MenuProps {}

export class Menu extends React.Component<MenuProps, any> {
  constructor(props: MenuProps) {
    super(props);

    this.state = {
      isOpen: false,
    };

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }

  renderMenu() {
    return (
      <div className="menu-items">
        <Link onClick={this.handleClick} className="menu-item" to="/">
          Trade
        </Link>
        <Link onClick={this.handleClick} className="menu-item" to="/accounts">
          Accounts
        </Link>
        <Link onClick={this.handleClick} className="menu-item" to="/activity">
          Activity
        </Link>
      </div>
    );
  }

  render() {
    return (
      <div className="Menu">
        <button type="button" className="Menu" onClick={this.handleClick}>
          <Icon className="more-icon" type="more" style={{ color: '#fff' }} />
        </button>
        {this.state.isOpen ? this.renderMenu() : null}
        {this.state.isOpen ? <div onClick={this.handleClick} className="overlay" /> : null}
      </div>
    );
  }
}
