import axios, { AxiosError } from 'axios';
import {
  SuccessResponse,
  ErrorResponse,
  Response,
  LedgerAccount,
  RippledErrorResponse,
} from '../entities';

export interface SpotPrice {
  market: string;
  price: string;
}

export function normalizeRippledError(error: any): RippledErrorResponse {
  return {
    name: error.name ? error.name : 'SocketError',
    message: error.message ? error.message : '',
  };
}

export function createResponseSuccess<T>(data: T): SuccessResponse<T> {
  return { success: true, data };
}

export function handleErrorResponse(error: AxiosError): ErrorResponse {
  return {
    success: false,
    code: error.response && error.response.status,
    message: error.message,
  };
}

export function createMockSuccessResponse(data: any) {
  return new Promise(resolve => setTimeout(resolve, 1000)).then(() => {
    return createResponseSuccess(data);
  });
}

export function getSpotPrice(): Promise<Response<SpotPrice>> {
  return axios({
    method: 'GET',
    url: 'https://api.coinfield.com/v1/tickers/xrpgbp',
  })
    .then(response => createResponseSuccess(response.data))
    .catch(error => handleErrorResponse(error));
}

export function createOrder(): Promise<Response<SpotPrice>> {
  return axios({
    method: 'POST',
    url: 'http://localhost:8000/pay/stripe',
    data: {
      stripeEmail: 'tpkidddle@gmail.com',
      stripeToken: 'asdasdasd',
      stripeTokenType: 'asdasdasd',
    },
  })
    .then(response => createResponseSuccess(response.data))
    .catch(error => handleErrorResponse(error));
}

export function getTestAccountCredentials(): Promise<Response<LedgerAccount>> {
  return axios({
    method: 'POST',
    url: 'https://faucet.altnet.rippletest.net/accounts',
  })
    .then(response => createResponseSuccess({ ...response.data.account }))
    .catch(error => handleErrorResponse(error));
}

export function getBalances(): Promise<Response<any>> {
  return axios({
    method: 'GET',
    url: '/v1/flywheel/account',
  })
    .then(response => createResponseSuccess(response.data))
    .catch(error => handleErrorResponse(error));
}

export function getSuggestionRates(): Promise<Response<any>> {
  return axios({
    method: 'GET',
    url: '/v1/flywheel/synthetic_rate?sourceCurrency=usd&destinationCurrency=mxn',
  })
    .then(response => createResponseSuccess(response.data))
    .catch(error => handleErrorResponse(error));
}

export function executeTrade(amount: string): Promise<Response<any>> {
  // return axios({
  //   method: 'POST',
  //   url: `/v1/flywheel/execute?xrpAmount=${amount}&sourceExchange=bitso&destinationExchange=bitstamp`,
  // })
  //   .then(response => createResponseSuccess(response.data))
  //   .catch(error => handleErrorResponse(error));
  return new Promise(resolve => setTimeout(resolve, 1000)).then(() => {
    return createMockSuccessResponse({ id: '13k1jkj31h4' });
  });
}

export function getTrade(id: string): Promise<Response<any>> {
  // return axios({
  //   method: 'GET',
  //   url: `/v1/flywheel/trade?paymentId=${id}`,
  // })
  //   .then(response => createResponseSuccess(response.data))
  //   .catch(error => handleErrorResponse(error));
  return new Promise(resolve => setTimeout(resolve, 1000)).then(() => {
    return createMockSuccessResponse({ status: 'COMPLETE' });
  });
}
