import * as React from 'react';

import './styles.css';

interface GhostButtonProps {
  disabled?: boolean;
  onClick?: (e: React.MouseEvent<HTMLButtonElement>) => void;
}

export class GhostButton extends React.Component<GhostButtonProps> {
  constructor(props: GhostButtonProps) {
    super(props);
  }

  render() {
    return (
      <button className="GhostButton" disabled={this.props.disabled} onClick={this.props.onClick}>
        {this.props.children}
      </button>
    );
  }
}
