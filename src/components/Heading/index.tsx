import * as React from 'react';

import './styles.css';

interface HeadingProps {}

export class Heading extends React.Component<HeadingProps> {
  constructor(props: HeadingProps) {
    super(props);
  }

  render() {
    return <h1 className="Heading">{this.props.children}</h1>;
  }
}
