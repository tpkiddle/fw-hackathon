import { actionTypes } from './actionTypes';
import { RegisteredAction } from 'src/actions';
import { FetchedData } from 'src/entities';
import { createFetchedData } from 'src/utils/misc';

export interface SuggestionsPageContainerState {
  rates: FetchedData<any>;
  trade: FetchedData<any>;
  tradeCheck: FetchedData<any>;
}

export const initialState: SuggestionsPageContainerState = {
  rates: createFetchedData(),
  trade: createFetchedData(),
  tradeCheck: createFetchedData(),
};

export function reducer(
  state: SuggestionsPageContainerState = initialState,
  action: RegisteredAction
) {
  switch (action.type) {
    case actionTypes.GET_RATES_START:
      return {
        ...state,
        rates: {
          isLoading: true,
          error: null,
          data: null,
        },
      };
    case actionTypes.GET_RATES_DONE:
      return {
        ...state,
        rates: {
          isLoading: false,
          error: null,
          data: action.rates,
        },
      };
    case actionTypes.GET_RATES_FAIL:
      return {
        ...state,
        rates: {
          isLoading: false,
          error: action.error,
          data: null,
        },
      };
    case actionTypes.EXECUTE_START:
      return {
        ...state,
        trade: {
          isLoading: true,
          error: null,
          data: null,
        },
      };
    case actionTypes.EXECUTE_DONE:
      return {
        ...state,
        trade: {
          isLoading: false,
          error: null,
          data: action.trade,
        },
      };
    case actionTypes.EXECUTE_FAIL:
      return {
        ...state,
        trade: {
          isLoading: false,
          error: action.error,
          data: null,
        },
      };
    case actionTypes.GET_TRADE_START:
      return {
        ...state,
        tradeCheck: {
          isLoading: true,
          error: null,
          data: null,
        },
      };
    case actionTypes.GET_TRADE_DONE:
      return {
        ...state,
        tradeCheck: {
          isLoading: false,
          error: null,
          data: action.tradeCheck,
        },
      };
    case actionTypes.GET_TRADE_FAIL:
      return {
        ...state,
        tradeCheck: {
          isLoading: false,
          error: action.error,
          data: null,
        },
      };
    case actionTypes.CLEAR_TRADE:
      return {
        ...state,
        tradeCheck: {
          isLoading: false,
          error: null,
          data: action.data,
        },
        trade: {
          isLoading: false,
          error: null,
          data: action.data,
        },
      };
    default:
      return state;
  }
}
