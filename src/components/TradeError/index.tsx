import * as React from 'react';

import './styles.css';
import { Heading } from '../Heading';

interface TradeCompleteProps {}

export class TradeComplete extends React.Component<TradeCompleteProps> {
  constructor(props: TradeCompleteProps) {
    super(props);
  }

  render() {
    return (
      <React.Fragment>
        <div className={`TradeComplete`}>
          <img className="round-tick-icon" src="images/complete.svg" />
          <Heading>
            Congratulations! <br />
            Your trade is now complete.
          </Heading>
        </div>
      </React.Fragment>
    );
  }
}
