import * as React from 'react';
import './styles.css';
import 'antd/dist/antd.css';
import { ROUTES_CONFIG } from 'src/utils/routesConfig';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { RouteNotFound } from 'src/components/RouteNotFound';
import { FetchedData, LedgerAccount } from 'src/entities';

export interface AppRootProps {
  isConnected: boolean;
  account: FetchedData<LedgerAccount>;
}

export interface AppRootState {}

export class AppRoot extends React.Component<AppRootProps, AppRootState> {
  renderRoutes() {
    const routes = Object.keys(ROUTES_CONFIG);

    return routes.map((path: string) => {
      const config = ROUTES_CONFIG[path];
      const component = config.component;

      if (!component) {
        throw new Error('Page component does not exist in Routes Config.');
      }
      return <Route exact path={path} component={component} key={path} />;
    });
  }

  render() {
    return (
      <Router>
        <Switch>
          {this.renderRoutes()}
          <Route component={RouteNotFound} />
        </Switch>
      </Router>
    );
  }
}
